# Valideur de flux xml

## I - Présentation

Un fichier xsd (xml schéma definition) est un fichier définissant les éléments pouvant être présent dans un flux xml, il y définit également les attributs et les différentes règles des éléments qui le compose.

Ce projet permet de tester si un flux xml respecte bien un xsd donné et d'avoir un retour sur les erreurs présentes si le xml n'est pas valide.

## II - Utilisation

>Pour être utilisé ce projet nécessite php.

Pour tester si un flux xml respecte bien un xsd, commencer par placer votre flux xml dans le dossier **flux_xml**, ensuite placer votre xsd dans le dossier **XSD**. Enfin, ouvrir un terminal à la racine du projet et utiliser la commande :

>php valideur_xml.php <**nom du fichier xml**> <**nom du fichier xsd**>

### Exemple :

Si mon flux xml s'appelle **lheo_offreinfo.xml** et mon xsd **lheo2.3.xsd** alors la commande sera :

>php valideur_xml.php lheo_offreinfo lheo2.3

Par défaut ce projet contient deux fichiers xsd, un pour lheo2.2 et un pour lheo2.3, permettant de tester si un flux respecte la version 2.2 ou 2.3 du langage lhéo.

## III - Démonstration

Voici le genre de résultat que peut donner ce valideur de flux :

![screen terminal](https://www.pixenli.com/image/Si4o3ZGb)

Ici le script est appelé une première fois sur un fichier valide, le retour indique donc que le schéma est totalement validé.

La deuxième fois il y a des erreurs retournées, un siret trop long ligne 4456835 et un code financeur non prévu ligne 8552540.