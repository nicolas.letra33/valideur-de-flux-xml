<?php

$XMLverif = new DOMDocument();
$XMLverif->load("./flux_xml/".$argv[1].".xml", LIBXML_BIGLINES);

libxml_use_internal_errors(true);

if ($XMLverif->schemaValidate("./XSD/".$argv[2].".xsd")) {
    echo("Le schéma est totalement validé\n");
}
else{
    $errors = libxml_get_errors();
    foreach ($errors as $error) {
        echo("Erreur : ".$error->code);
        echo(" - ".$error->message);
        echo("Ligne : ".$error->line."\n\n");
    }
    echo("xml non conforme au xsd\n");
}